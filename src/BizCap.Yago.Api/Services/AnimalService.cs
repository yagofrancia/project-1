using System.Collections.Generic;
using BizCap.Yago.Commons.ValueObjects;
using BizCap.Yago.Commons.Extensions;
using BizCap.Yago.Domain.Enumerateds;
using BizCap.Yago.Commons.Generators;

namespace BizCap.Yago.Api.Services
{
    public class AnimalService : IAnimalService
    {
        public IEnumerable<AnimalVO> GetRandomAnimals(int pageLength)
        {
            var animals = new List<AnimalVO>();

            for (int i = 0; i < pageLength; i++)
            {
                var generator = GetRandomAnimalGenerator();
                var animal = generator.GenerateAnimal();
                animals.Add(animal);
            }
            return animals;
        }

        private AnimalGenerator GetRandomAnimalGenerator()
        {
            var type = typeof(AnimalType).GetRandomValue();

            return type switch
            {
                AnimalType.CAT => new CatGenerator(),
                AnimalType.DOG => new DogGenerator(),
                AnimalType.BIRD => new BirdGenerator(),
                AnimalType.HORSE => new HorseGenerator(),
                _ => new CatGenerator()
            };
        }
    }
}