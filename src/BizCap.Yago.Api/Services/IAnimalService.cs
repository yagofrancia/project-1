using System;
using System.Collections.Generic;
using BizCap.Yago.Commons.ValueObjects;
using BizCap.Yago.Domain.Enumerateds;

namespace BizCap.Yago.Api.Services
{
    public interface IAnimalService
    {
        IEnumerable<AnimalVO> GetRandomAnimals(int pageLength);
    }
}