namespace BizCap.Yago.Api.Requests {
    public class GetRandomAnimalsRequest {
        public int PageLength { get; set; }
    }
}