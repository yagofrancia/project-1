using System.Collections.Generic;
using BizCap.Yago.Commons.ValueObjects;

namespace BizCap.Yago.Api.Responses
{
    public class GetRandomAnimalsResponse
    {
        public IEnumerable<object> Data { get; set; }
    }
}