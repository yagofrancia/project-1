﻿using Microsoft.AspNetCore.Mvc;
using BizCap.Yago.Api.Requests;
using BizCap.Yago.Api.Responses;
using BizCap.Yago.Api.Services;

namespace BizCap.Yago.Api.Controllers
{
    [ApiController]
    [Route("animals")]
    public class AnimalController : ControllerBase
    {

        private readonly IAnimalService _animalService;

        public AnimalController(IAnimalService animalService) => _animalService = animalService;

        [HttpGet]
        public GetRandomAnimalsResponse GetRandomAnimals([FromQuery] GetRandomAnimalsRequest request)
        {
            var pageLength = request.PageLength != 0 ? request.PageLength : 20;
            var animals = _animalService.GetRandomAnimals(pageLength);

            return new GetRandomAnimalsResponse
            {
                Data = animals
            };
        }
    }
}
