namespace BizCap.Yago.Domain.Enumerateds
{
    public enum AnimalWingSize
    {
        SMALL,
        MEDIUM,
        BIG
    }
}