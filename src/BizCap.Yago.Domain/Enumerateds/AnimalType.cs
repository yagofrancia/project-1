namespace BizCap.Yago.Domain.Enumerateds
{
    public enum AnimalType
    {
        DOG,
        CAT,
        BIRD,
        HORSE,
    }
}