namespace BizCap.Yago.Domain.Enumerateds
{
    public enum DogBreed
    {
        MUTT,
        PODDLE,
        DACHSCHUND,
        GOLDEN_RETRIEVER,
        BULLDOG,
        PUG,
        SHITZU,
        CHIHUAHUA,
        SHIBA_INU
    }
}