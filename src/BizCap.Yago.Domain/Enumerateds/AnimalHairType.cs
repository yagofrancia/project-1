namespace BizCap.Yago.Domain.Enumerateds
{
    public enum AnimalHairType
    {
        LONG,
        SHORT,
        CURLY,
        HAIRLESS
    }
}