namespace BizCap.Yago.Domain.Enumerateds
{
    public enum AnimalColor
    {
        YELLOW,
        BROWN,
        WHITE,
        BLACK,
        GREY,
    }
}