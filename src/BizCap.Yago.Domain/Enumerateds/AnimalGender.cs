namespace BizCap.Yago.Domain.Enumerateds
{
    public enum AnimalGender
    {
        MALE,
        FEMALE
    }
}