namespace BizCap.Yago.Domain.Enumerateds
{
    public enum AnimalSize
    {
        SMALL,
        MEDIUM,
        BIG
    }
}