using BizCap.Yago.Commons.Extensions;
using BizCap.Yago.Commons.ValueObjects;
using BizCap.Yago.Domain.Enumerateds;

namespace BizCap.Yago.Commons.Generators
{
    public class CatGenerator : AnimalGenerator
    {
        public override AnimalVO GenerateAnimal()
        {
            return new CatVO
            {
                HairType = (AnimalHairType)typeof(AnimalHairType).GetRandomValue(),
            };
        }
    }
}