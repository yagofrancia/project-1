using BizCap.Yago.Commons.Extensions;
using BizCap.Yago.Commons.ValueObjects;
using BizCap.Yago.Domain.Enumerateds;

namespace BizCap.Yago.Commons.Generators
{
    public class DogGenerator : AnimalGenerator
    {
        public override AnimalVO GenerateAnimal()
        {
            return new DogVO
            {
                Breed = (DogBreed)typeof(DogBreed).GetRandomValue(),
            };
        }
    }
}