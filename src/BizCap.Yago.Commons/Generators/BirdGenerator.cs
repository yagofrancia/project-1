using BizCap.Yago.Commons.Extensions;
using BizCap.Yago.Commons.ValueObjects;
using BizCap.Yago.Domain.Enumerateds;

namespace BizCap.Yago.Commons.Generators
{
    public class BirdGenerator : AnimalGenerator
    {
        public override AnimalVO GenerateAnimal()
        {
            return new BirdVO
            {
                WingSize = (AnimalWingSize)typeof(AnimalWingSize).GetRandomValue(),
            };
        }
    }
}