using BizCap.Yago.Commons.ValueObjects;

namespace BizCap.Yago.Commons.Generators {
    public abstract class AnimalGenerator {
        public abstract AnimalVO GenerateAnimal();
    }
}