using System;
using System.Linq;
using BizCap.Yago.Domain.Enumerateds;

namespace BizCap.Yago.Commons.Extensions
{
    public static class EnumExtensions
    {
        public static Enum GetRandomValue(this Type type)
        {
            return Enum.GetValues(type)
                .OfType<Enum>()
                .OrderBy(e => Guid.NewGuid())
                .FirstOrDefault();
        }
    }
}