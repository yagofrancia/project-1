namespace BizCap.Yago.Commons.ValueObjects
{
    public abstract class DomesticAnimalVO : AnimalVO
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public abstract void Play();

        private void Walk()
        {

        }
        public override void Move()
        {
            Walk();
        }
    }
}