using System;
using System.Linq;
using System.Text.Json.Serialization;
using BizCap.Yago.Domain.Enumerateds;

namespace BizCap.Yago.Commons.ValueObjects
{
    public class CatVO : DomesticAnimalVO
    {
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public override AnimalType Type => AnimalType.CAT;

        [JsonConverter(typeof(JsonStringEnumConverter))]
        public AnimalHairType HairType { get; set; }

        public override void MakeSound()
        {
            Meow();
        }

        public override void Play()
        {
            GrabYarn();
        }

        private void Meow()
        {

        }

        private void GrabYarn() {

        }
    }
}