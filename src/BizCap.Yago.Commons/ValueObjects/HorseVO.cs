using System.Text.Json.Serialization;
using BizCap.Yago.Domain.Enumerateds;

namespace BizCap.Yago.Commons.ValueObjects
{
    public class HorseVO : DomesticAnimalVO
    {
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public override AnimalType Type => AnimalType.HORSE;

        public override void MakeSound()
        {
            Neigh();
        }

        public override void Play()
        {
            Roll();
        }

        private void Neigh()
        {

        }

        private void Roll() {

        }
    }
}