using BizCap.Yago.Commons.Extensions;
using BizCap.Yago.Domain.Enumerateds;
using System.Text.Json.Serialization;

namespace BizCap.Yago.Commons.ValueObjects
{
    public abstract class AnimalVO
    {
        public abstract AnimalType Type { get; }

        [JsonConverter(typeof(JsonStringEnumConverter))]
        public AnimalColor Color { get; set; } = (AnimalColor)typeof(AnimalColor).GetRandomValue();

        [JsonConverter(typeof(JsonStringEnumConverter))]
        public AnimalSize Size { get; set; } = (AnimalSize)typeof(AnimalSize).GetRandomValue();

        [JsonConverter(typeof(JsonStringEnumConverter))]
        public AnimalWeight Weight { get; set; } = (AnimalWeight)typeof(AnimalWeight).GetRandomValue();

        [JsonConverter(typeof(JsonStringEnumConverter))]
        public AnimalGender Gender { get; set; } = (AnimalGender)typeof(AnimalGender).GetRandomValue();

        public abstract void MakeSound();

        public abstract void Move();

        public string NameAsString

        => Type switch
        {
            AnimalType.BIRD => "Passaro",
            AnimalType.DOG => "Cachorro",
            AnimalType.CAT => "Gato",
            AnimalType.HORSE => "Cavalo",
            _ => "Desconhecido"
        };

    }
}