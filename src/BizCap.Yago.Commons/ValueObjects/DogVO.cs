using System.Text.Json.Serialization;
using BizCap.Yago.Domain.Enumerateds;

namespace BizCap.Yago.Commons.ValueObjects
{
    public class DogVO : DomesticAnimalVO
    {
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public override AnimalType Type => AnimalType.DOG;

        [JsonConverter(typeof(JsonStringEnumConverter))]
        public DogBreed Breed { get; set; }

        public override void MakeSound()
        {
            Bark();
        }

        public override void Play()
        {
            CatchTheDisk();
        }

        private void Bark()
        {

        }

        private void CatchTheDisk() {

        }
    }
}