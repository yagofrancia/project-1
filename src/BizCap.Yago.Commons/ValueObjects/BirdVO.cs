using System;
using System.Text.Json.Serialization;
using BizCap.Yago.Domain.Enumerateds;

namespace BizCap.Yago.Commons.ValueObjects
{
    public class BirdVO : AnimalVO
    {
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public override AnimalType Type => AnimalType.BIRD;

        [JsonConverter(typeof(JsonStringEnumConverter))]
        public AnimalWingSize WingSize { get; set; }
        public int FlightSpeed { get; set; }

        private void Fly()
        {
            throw new NotImplementedException();
        }

        public override void Move()
        {
            Fly();
        }

        public override void MakeSound()
        {
            Croak();
        }

        private void Croak()
        {

        }

    }
}